# Generated by Django 2.2.7 on 2019-12-04 05:36

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('order_app', '0002_auto_20191202_2322'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pizzaorder',
            name='number',
            field=models.IntegerField(default=1),
        ),
    ]
